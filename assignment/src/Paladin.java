
public class Paladin extends Knight {

	public Paladin(int baseHp, int wp) {
		super(baseHp, wp);
		super.setBaseHp(baseHp);
		super.setWp(wp);
	}

	public double getCombatScore() {
		if(isFibo(super.getBaseHp()))return 1000+FiboToInt(super.getBaseHp());
		else return super.getBaseHp()*3;
	}
	
	public boolean isFibo(int number) {
		int i=2,j=0;
		while(j<number) {
			j=fibo(i);
			i++;
		}
		if(j==number)return true;
		else return false;
	}
	
	public int FiboToInt(int number) {
		int i=2,j=0;
		while(j<number) {
			j=fibo(i);
			i++;
		}
		if(j==number)return j;
		else return 0;
	}
	
	public int fibo(int number) {
		if(number==0)return 0;
		else if(number==1)return 1;
		else return fibo(number-1)+fibo(number-2);
	}
}
