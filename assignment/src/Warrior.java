public class Warrior extends Fighter {

	public Warrior(int baseHp, int wp) {
		super(baseHp, wp);
		super.setBaseHp(baseHp);
		super.setWp(wp);
	}

	@Override
	public double getCombatScore() {
		if(super.getWp()==1)return super.getBaseHp();
		else return super.getBaseHp()/10;
	}
    
}
