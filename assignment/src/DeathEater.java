public class DeathEater extends Monster implements Combatable{

	public DeathEater(Complex mana) {
		super(mana);
		super.setMana(mana);
	}

	@Override
	public double getCombatScore() {
		return super.getMana().getMagnitude();
	}

	

}
